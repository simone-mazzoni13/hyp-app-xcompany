class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.date :date
      t.time :time
      t.string :place
      t.string :region
      t.string :photo_url

      t.timestamps
    end
  end
end
