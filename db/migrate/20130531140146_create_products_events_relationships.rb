class CreateProductsEventsRelationships < ActiveRecord::Migration
  def change
    create_table :products_events_relationships do |t|
      t.integer :product_id
      t.integer :event_id

      t.timestamps
    end

    add_index :products_events_relationships, :product_id
    add_index :products_events_relationships, :event_id
    add_index :products_events_relationships, [:product_id, :event_id], unique: true, :name => 'index_products_events'

  end
end
