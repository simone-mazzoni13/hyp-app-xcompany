class CreateProducts < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.string :name
      t.text :description
      t.boolean :home_office
      t.string :category
      t.boolean :partner
      t.text :services
      t.boolean :top

      t.timestamps

     # t.rename :type, :category
    end
  end
end
