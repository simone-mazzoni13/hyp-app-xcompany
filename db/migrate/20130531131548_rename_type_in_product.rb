class RenameTypeInProduct < ActiveRecord::Migration
  def change
    rename_column :products, :type, :category
  end

  def down
  end
end
