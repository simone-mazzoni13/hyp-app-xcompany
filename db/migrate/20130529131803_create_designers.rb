class CreateDesigners < ActiveRecord::Migration
  def change
    create_table :designers do |t|
      t.string :name
      t.string :lastname
      t.date :birthday
      t.text :biography
      t.text :awards
      t.string :photo_url

      t.timestamps
    end
  end
end
