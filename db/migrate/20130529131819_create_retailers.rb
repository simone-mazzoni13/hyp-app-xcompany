class CreateRetailers < ActiveRecord::Migration
  def change
    create_table :retailers do |t|
      t.string :name
      t.text :description
      t.string :address
      t.string :region
      t.string :phone_number
      t.string :mail
      t.string :photo_url

      t.timestamps
    end
  end
end
