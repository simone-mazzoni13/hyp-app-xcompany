class CreateProductsRetailersRelationships < ActiveRecord::Migration
  def change
    create_table :products_retailers_relationships do |t|
      t.integer :product_id
      t.integer :retailer_id

      t.timestamps
    end

    add_index :products_retailers_relationships, :product_id
    add_index :products_retailers_relationships, :retailer_id
    add_index :products_retailers_relationships, [:product_id, :retailer_id], unique: true, :name => 'index_products_retailers'
    add_index :products_designers_relationships, [:product_id, :designer_id], unique: true, :name => 'index_products_designers'

  end
end
