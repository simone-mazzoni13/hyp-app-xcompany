# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130531143434) do

  create_table "designers", :force => true do |t|
    t.string   "name"
    t.string   "lastname"
    t.date     "birthday"
    t.text     "biography"
    t.text     "awards"
    t.string   "photo_url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.date     "date"
    t.time     "time"
    t.string   "place"
    t.string   "region"
    t.string   "photo_url"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "pictures", :force => true do |t|
    t.string   "url_image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "product_id"
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "home_office"
    t.string   "category"
    t.boolean  "partner"
    t.text     "services"
    t.boolean  "top"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "products_designers_relationships", :force => true do |t|
    t.integer  "product_id"
    t.integer  "designer_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "products_designers_relationships", ["designer_id"], :name => "index_products_designers_relationships_on_designer_id"
  add_index "products_designers_relationships", ["product_id", "designer_id"], :name => "index_products_designers", :unique => true
  add_index "products_designers_relationships", ["product_id"], :name => "index_products_designers_relationships_on_product_id"

  create_table "products_events_relationships", :force => true do |t|
    t.integer  "product_id"
    t.integer  "event_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "products_events_relationships", ["event_id"], :name => "index_products_events_relationships_on_event_id"
  add_index "products_events_relationships", ["product_id", "event_id"], :name => "index_products_events", :unique => true
  add_index "products_events_relationships", ["product_id"], :name => "index_products_events_relationships_on_product_id"

  create_table "products_retailers_relationships", :force => true do |t|
    t.integer  "product_id"
    t.integer  "retailer_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "products_retailers_relationships", ["product_id", "retailer_id"], :name => "index_products_retailers", :unique => true
  add_index "products_retailers_relationships", ["product_id"], :name => "index_products_retailers_relationships_on_product_id"
  add_index "products_retailers_relationships", ["retailer_id"], :name => "index_products_retailers_relationships_on_retailer_id"

  create_table "retailers", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "address"
    t.string   "region"
    t.string   "phone_number"
    t.string   "mail"
    t.string   "photo_url"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "retailers_events_relationships", :force => true do |t|
    t.integer  "retailer_id"
    t.integer  "event_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "retailers_events_relationships", ["event_id"], :name => "index_retailers_events_relationships_on_event_id"
  add_index "retailers_events_relationships", ["retailer_id", "event_id"], :name => "index_retailers_events", :unique => true
  add_index "retailers_events_relationships", ["retailer_id"], :name => "index_retailers_events_relationships_on_retailer_id"

end
