require 'test_helper'

class EventsControllerTest < ActionController::TestCase
  test "should get by_date" do
    get :by_date
    assert_response :success
  end

  test "should get by_region" do
    get :by_region
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

end
