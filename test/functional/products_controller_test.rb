require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  test "should get office_by_type" do
    get :office_by_type
    assert_response :success
  end

  test "should get home_by_type" do
    get :home_by_type
    assert_response :success
  end

  test "should get by_partners" do
    get :by_partners
    assert_response :success
  end

  test "should get top" do
    get :top
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

end
