class ProductsRetailersRelationship < ActiveRecord::Base
  attr_accessible :product_id, :retailer_id

  belongs_to :product, class_name: "Product"
  belongs_to :retailer, class_name: "Retailer"

  validates :retailer_id, presence: true
  validates :product_id, presence: true
end
