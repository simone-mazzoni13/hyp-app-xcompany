class ProductsDesignersRelationship < ActiveRecord::Base
  attr_accessible :designer_id, :product_id
  
  belongs_to :product, class_name: "Product"
  belongs_to :designer, class_name: "Designer"

  validates :designer_id, presence: true
  validates :product_id, presence: true

end
