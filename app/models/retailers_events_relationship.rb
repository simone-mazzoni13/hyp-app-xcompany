class RetailersEventsRelationship < ActiveRecord::Base
  attr_accessible :event_id, :retailer_id

  belongs_to :retailer, class_name: "Retailer"
  belongs_to :event, class_name: "Event"

  validates :event_id, presence: true
  validates :retailer_id, presence: true

end
