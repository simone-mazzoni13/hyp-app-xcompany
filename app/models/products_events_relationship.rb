class ProductsEventsRelationship < ActiveRecord::Base
  attr_accessible :event_id, :product_id

  belongs_to :product, class_name: "Product"
  belongs_to :event, class_name: "Event"

  validates :event_id, presence: true
  validates :product_id, presence: true
end
