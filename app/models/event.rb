class Event < ActiveRecord::Base
  attr_accessible :date, :description, :name, :photo_url, :place, :region, :time

  has_many :reverse_products_events_relationships, foreign_key: "event_id", class_name:  "ProductsEventsRelationship", dependent:   :destroy
  has_many :products, through: :reverse_products_events_relationships, source: :product

  has_many :reverse_retailers_events_relationships, foreign_key: "event_id", class_name:  "RetailersEventsRelationship", dependent:   :destroy
  has_many :retailers, through: :reverse_retailers_events_relationships, source: :retailer

  validates :name, :presence => true
  validates :description, :presence => true
  validates :place, :presence => true
  validates :region, :presence => true
  validates :date, :presence => true

end
