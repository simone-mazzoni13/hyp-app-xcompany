class Product < ActiveRecord::Base
  attr_accessible :description, :home_office, :name, :partner, :services, :top, :category, :designers

  validates :name, :presence => true
  validates :description, :presence => true
  validates :services, :presence => true
  validates :category, :presence => true
  validates :home_office, :presence => true

  has_many :products_designers_relationships, foreign_key: "product_id", dependent: :destroy
  has_many :designers, through: :products_designers_relationships, source: :designer

  has_many :products_retailers_relationships, foreign_key: "product_id", dependent: :destroy
  has_many :retailers, through: :products_retailers_relationships, source: :retailer

  has_many :products_events_relationships, foreign_key: "product_id", dependent: :destroy
  has_many :events, through: :products_events_relationships, source: :event

  has_many :pictures, foreign_key: "product_id", dependent: :destroy

end
