class EventsController < ApplicationController
  def by_date
    @all_events = Event.all
    @dates = []
    @all_events.each do |event|
      @dates << event.date
    end
    @dates.uniq!
    @dates.sort_by! {|date| date}
  end

  def by_region
    @all_events = Event.all
    @regions = []
    @all_events.each do |event|
      @regions << event.region
    end
    @regions.uniq!
  end

  def show
    @event = Event.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]

    if @des
      @t = Designer.find(@des)
      @desname = @t.name + @t.lastname
    end

    if @pro and @pro != '0'
      @t = Product.find(@pro)
      @proname = @t.name
    end

    @path_left = case
                   when @reg
                     @e = Event.where('region = ?', @reg)
                     hash = Hash[@e.map.with_index.to_a]
                     @i = hash[@event]
                     @m = @e.count

                     (@i == 0 ? events_show_path(:id => @e[@m - 1].id, :reg => @reg) : events_show_path(:id => @e[@i.to_i - 1].id, :reg => @reg))
                   when @dat
                     @e = Event.where('date = ?', @dat)
                     hash = Hash[@e.map.with_index.to_a]
                     @i = hash[@event]
                     @m = @e.count

                     (@i == 0 ? events_show_path(:id => @e[@m - 1].id, :dat => @dat) : events_show_path(:id => @e[@i.to_i - 1].id, :dat => @dat))
                   else
                 end

    @path_up = case
                 when @reg
                   events_by_region_path
                 when @dat
                   events_by_date_path
                 when @pro
                   products_show_path(:id => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall)
                 when @ret
                   retailers_show_path(:id => @ret, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall)
                 else
               end

    @path_right = case
                   when @reg
                     @e = Event.where('region = ?', @reg)
                     hash = Hash[@e.map.with_index.to_a]
                     @i = hash[@event]
                     @m = @e.count

                     (@i == (@m - 1) ? events_show_path(:id => @e[0].id, :reg => @reg) : events_show_path(:id => @e[@i.to_i + 1].id, :reg => @reg))
                   when @dat
                     @e = Event.where('date = ?', @dat)
                     hash = Hash[@e.map.with_index.to_a]
                     @i = hash[@event]
                     @m = @e.count

                     (@i == (@m - 1) ? events_show_path(:id => @e[0].id, :dat => @dat) : events_show_path(:id => @e[@i.to_i + 1].id, :dat => @dat))
                   else
                 end
  end

  def events_retailers
    @event = Event.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]
  end

  def events_products
    @event = Event.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]
  end

  def new
    @event = Event.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @event }
    end
  end

  def create
    @event = Event.new(params[:event])

    @event.photo_url = "eventdefault.jpg"

    respond_to do |format|
      if @event.save
        format.html { redirect_to pages_admin_path, :notice => 'Event was successfully created.' }
      else
        format.html { render :action => "new" }
      end
    end

  end
end
