class DesignersController < ApplicationController
  def all
    @all_designers = Designer.all
  end

  def index
    @all_designers = Designer.all

    redirect_to designers_all_path
  end

  def show
    @designer = Designer.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]

    @max_des_id = Designer.maximum("id")

    if @designer.id - 1 == 0
      @prec_des = @max_des_id
    else
      @prec_des = @designer.id - 1
    end

    if @designer.id == @max_des_id
      @succ_des = 1
    else
      @succ_des = @designer.id + 1
    end
  end

  def awards
    @designer = Designer.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]

    @max_des_id = Designer.maximum("id")

    if @designer.id - 1 == 0
      @prec_des = @max_des_id
    else
      @prec_des = @designer.id - 1
    end

    if @designer.id == @max_des_id
      @succ_des = 1
    else
      @succ_des = @designer.id + 1
    end
  end

  def new
    @designer = Designer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @designer }
    end
  end

  def create
    @designer = Designer.new(params[:designer])

    @designer.photo_url = "designerdefault.jpg"

    respond_to do |format|
      if @designer.save
        format.html { redirect_to pages_admin_path, :notice => 'Designer was successfully created.' }
      else
        format.html { render :action => "new" }
      end
    end
  end

end
