class PagesController < ApplicationController
  def home
    render :layout => false
  end

  def who_we_are
  end

  def where_we_are
  end

  def contact_us
  end

  def work_with_us
  end

  def partners
  end

  def services
  end

  def admin
  end

  def new_product_designer_relation
    @products = Product.all
    @designers = Designer.all

  end

  def create_product_designer_relation
    @product_id = params[:product]
    @designer_id = params[:designer]

    @rel = ProductsDesignersRelationship.new(product_id:@product_id, designer_id:@designer_id)

    if ( ProductsDesignersRelationship.where('product_id = ? AND designer_id = ?', params[:product], params[:designer]).first )
      respond_to do |format|
      format.html { redirect_to pages_admin_path, :notice => 'The association already exists.' }
      end
    else
      respond_to do |format|
        if @rel.save
          format.html { redirect_to pages_admin_path, :notice => 'Relation was successfully created.' }
        else
          format.html { redirect_to pages_new_product_designer_relation_path, :notice => 'Creation failed.' }
        end
      end
    end

  end

  def new_product_retailer_relation
    @products = Product.all
    @retailers = Retailer.all

  end

  def create_product_retailer_relation
    @product_id = params[:product]
    @retailer_id = params[:retailer]

    @rel = ProductsRetailersRelationship.new(product_id:@product_id, retailer_id:@retailer_id)

    if ( ProductsRetailersRelationship.where('product_id = ? AND retailer_id = ?', params[:product], params[:retailer]).first )
      respond_to do |format|
        format.html { redirect_to pages_admin_path, :notice => 'The association already exists.' }
      end
    else
      respond_to do |format|
      if @rel.save
        format.html { redirect_to pages_admin_path, :notice => 'Relation was successfully created.' }
      else
        format.html { redirect_to pages_new_product_retailer_relation_path, :notice => 'Creation failed.' }
      end
      end
    end
  end

  def new_product_event_relation
    @products = Product.all
    @events = Event.all

  end

  def create_product_event_relation
    @product_id = params[:product]
    @event_id = params[:event]

    @rel = ProductsEventsRelationship.new(product_id:@product_id, event_id:@event_id)

    if ( ProductsEventsRelationship.where('product_id = ? AND event_id = ?', params[:product], params[:event]).first )
      respond_to do |format|
        format.html { redirect_to pages_admin_path, :notice => 'The association already exists.' }
      end
    else
      respond_to do |format|
      if @rel.save
        format.html { redirect_to pages_admin_path, :notice => 'Relation was successfully created.' }
      else
        format.html { redirect_to pages_new_product_event_relation_path, :notice => 'Creation failed.' }
      end
      end
    end
  end

  def new_retailer_event_relation
    @retailers = Retailer.all
    @events = Event.all

  end

  def create_retailer_event_relation
    @retailer_id = params[:retailer]
    @event_id = params[:event]

    @rel = RetailersEventsRelationship.new(retailer_id:@retailer_id, event_id:@event_id)

    if ( RetailersEventsRelationship.where('retailer_id = ? AND event_id = ?', params[:retailer], params[:event]).first )
      respond_to do |format|
        format.html { redirect_to pages_admin_path, :notice => 'The association already exists.' }
      end
    else
      respond_to do |format|
      if @rel.save
        format.html { redirect_to pages_admin_path, :notice => 'Relation was successfully created.' }
      else
        format.html { redirect_to pages_new_retailer_event_relation_path, :notice => 'Creation failed.' }
      end
      end
    end
  end


end
